package com.fengyunhe.utils.mail;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;



/**
 * 发邮件
 * 
 * @author YY
 * 
 */
public class MailSender {
  // ***********************                            使用演示
  // MailSender s = new MailSender();
  // s.setStmpServer("smtp.exmail.qq.com");
  // s.setServerUser("yang.yan@nowledgedata.com.cn");
  // s.setServerPassword("********"); //这里是密码
  // s.setFromEmail("yang.yan@nowledgedata.com.cn");
  // s.setFromName("杨焱");
  // s.setTitle("测试标题");
  // s.setHtmlContent("测试内容");
  // s.setTo(new String[] { "277160299@qq.com" });
  // try {
  // boolean ok = s.sendMail();
  // System.out.println(ok ? "成功" : "失败");
  // } catch (Exception e) {
  // e.printStackTrace();
  // }
  private String stmpServer = null;
  private String serverUser = null;
  private String serverPassword = null;
  private String fromEmail = null;
  private String fromName = null;

  private String title = null;
  private String htmlContent = null;
  private String contentCharset = null;
  private String[] to;
  private int serverPort = -1;

  public boolean sendMail() throws Exception {
    try {
      Properties props = new Properties();// 通过什么协议发送邮件
      props.put("mail.smtp.host", stmpServer);
      props.put("mail.smtp.auth", "true");
      Session session = Session.getInstance(props, null);// 得到邮箱会话
      session.setDebug(true);// 得到错误信息
      MimeMessage message = new MimeMessage(session);// 得到邮件对象
      message.setFrom(new InternetAddress(fromEmail, fromName));// 得到发件人地址
      String toList = getMailList(getTo());
      InternetAddress[] iaToList = InternetAddress.parse(toList);
      message.addRecipients(javax.mail.Message.RecipientType.TO, iaToList);
      message.setSubject(getTitle());
      Multipart multipart = new MimeMultipart();
      BodyPart contentPart = new MimeBodyPart();
      contentPart.setContent(getHtmlContent(), "text/html;charset=" + (getContentCharset() == null ? "UTF-8" : getContentCharset()));
      multipart.addBodyPart(contentPart);
      message.setContent(multipart);
      message.saveChanges();
      Transport transport = session.getTransport("smtp");
      transport.connect(stmpServer, serverPort, serverUser, serverPassword);
      transport.sendMessage(message, message.getAllRecipients());
      transport.close();
      return true;
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return false;
  }

  private String getMailList(String[] mailArray) {

    StringBuffer toList = new StringBuffer();
    int length = mailArray.length;
    if (mailArray != null && length < 2) {
      toList.append(mailArray[0]);
    } else {
      for (int i = 0; i < length; i++) {
        toList.append(mailArray[i]);
        if (i != (length - 1)) {
          toList.append(",");
        }
      }  
    }
    return toList.toString();
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String[] getTo() {
    return to;
  }

  public void setTo(String[] to) {
    this.to = to;
  }

  public String getStmpServer() {
    return stmpServer;
  }

  public void setStmpServer(String stmpServer) {
    this.stmpServer = stmpServer;
  }

  public String getServerUser() {
    return serverUser;
  }

  public String getServerPassword() {
    return serverPassword;
  }

  public String getFromEmail() {
    return fromEmail;
  }

  public String getFromName() {
    return fromName;
  }

  public void setFromName(String fromName) {
    this.fromName = fromName;
  }

  public String getHtmlContent() {
    return htmlContent;
  }

  public void setHtmlContent(String htmlContent) {
    this.htmlContent = htmlContent;
  }

  public String getContentCharset() {
    return contentCharset;
  }

  public void setContentCharset(String contentCharset) {
    this.contentCharset = contentCharset;
  }

  public void setServerUser(String serverUser) {
    this.serverUser = serverUser;
  }

  public void setServerPassword(String serverPassword) {
    this.serverPassword = serverPassword;
  }

  public void setFromEmail(String fromEmail) {
    this.fromEmail = fromEmail;
  }

  public void setServerPort(int serverPort) {
    this.serverPort = serverPort;
  }
}
