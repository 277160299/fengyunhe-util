package com.fengyunhe.utils.http;

import java.io.IOException;
import java.util.Date;

/**
 * 根据不同的浏览器处理中文名
 * 
 * @author YY
 * 
 */
public class DownStreamUtil {
  // 根据不同的浏览器处理中文名
  public static String processFileName(String fileName, String agent) throws IOException {
    String codedfilename = null;
    // 取.前面的文件名
    String prefix = fileName.lastIndexOf(".") != -1 ? fileName.substring(0, fileName.lastIndexOf(".")) : fileName;
    // 取.后面的扩展名
    String extension = fileName.lastIndexOf(".") != -1 ? fileName.substring(fileName.lastIndexOf(".")) : "";
    // IE浏览器
    if (null != agent && -1 != agent.indexOf("MSIE")) {
      String name = java.net.URLEncoder.encode(fileName, "UTF8");
      // %0A为UTF8编码的结束符，如果存在截掉
      if (name.lastIndexOf("%0A") != -1) {
        name = name.substring(0, name.length() - 3);
      }
      // IE中，文件名成超过17个汉字
      int limit = 150 - extension.length();
      if (name.length() > limit) {
        name = java.net.URLEncoder.encode(prefix.substring(0, Math.min(prefix.length(), limit / 9)), "UTF-8");
        if (name.lastIndexOf("%0A") != -1) {
          name = name.substring(0, name.length() - 3);
        }
      }
      codedfilename = prefix + extension;
      // Safari浏览器 和 Chrome浏览器
    } else if (null != agent && -1 != agent.indexOf("Safari")) {
      // Chrome浏览器
      if (-1 != agent.indexOf("Chrome")) {
        codedfilename = "=?UTF-8?B?" + (new String(org.apache.commons.codec.binary.Base64.encodeBase64(fileName.getBytes("UTF-8")))) + "?=";
        // 暂时解决不了问题
      } else {
        codedfilename = new Date().getTime() + extension;
      }
      // FF浏览器
    } else if (null != agent && -1 != agent.indexOf("Firefox")) {
      codedfilename = "=?UTF-8?B?" + (new String(org.apache.commons.codec.binary.Base64.encodeBase64(fileName.getBytes("UTF-8")))) + "?=";
    } else {
      codedfilename = fileName;
    }
    return codedfilename;
  }
}
